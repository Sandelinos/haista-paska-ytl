#!/bin/sh
links -dump "$1" |\
sed '/Quellen/,$d' |\
sed '/Lähteet/,$d' |\
sed '/Sources/,$d' |\
sed '/Källor/,$d' |\
sed '/Источники/,$d' |\
sed '/Fontes/,$d' |\
sed '/Fuentes/,$d' |\
sed '/Kokeen tekstit pohjautuvat seuraaviin lähteisiin:/,$d' |\
sed "/I testi e le immagini dell'esame si basano sulle fonti seguenti:/,\$d" |\
grep -o "^\\s*[1-9]\\+\\.\\?[0-9]\\+\\?\\.\\?\\s" |\
#grep -o  "[1-9]\+\.\?[0-9]\?" |\
sed "s/^\\s\\+//g" |\
sort -g |\
uniq > "$2"
echo Done.
